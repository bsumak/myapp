const http = require('http');
const path = require('path');
const fs = require('fs');

const PORT = process.env.PORT || 3000;

const server = http.createServer((req, res) => {

    const potDoDatoteke = path.join(__dirname, 'www', (req.url === "/") ? "index.html" : req.url);

    let contentType = "text/html";

    switch (path.extname(potDoDatoteke)) {
        case '.css': contentType = 'text/css';
            break;
        case '.jpg': contentType = "image/jpg"
            break;
    }

    fs.readFile(potDoDatoteke, (err, data) => {

        if (err) {
            if (err.code === 'ENOENT') {
                fs.readFile(path.join(__dirname, 'www', '404.html'), (err2, data2) => {
                    res.writeHead(200, { 'Content-type': contentType });
                    res.end(data2);
                })
            } else {
                res.writeHead(500);
                res.end("Napaka");
            }
        } else {
            res.writeHead(200, { 'Content-type': contentType });
            res.end(data);
        }
    });

    // if (req.url==="/" || req.url==="/index.html"){
    //     res.writeHead(200, {'Content-type':'text/html'});
    //     res.end('<html><head><title>Node.js</title></head><body><h1>Prva stean</h1><p>....</p></body></html>')
    // }

    // if (req.url==="/about.html"){
    //     res.writeHead(200, {'Content-type':'text/html'});
    //     res.end('<html><head><title>Node.js</title></head><body><h1>About</h1><p>....</p></body></html>')
    // }

    // if (req.url==="/contact.html"){
    //     res.writeHead(200, {'Content-type':'text/html'});
    //     res.end('<html><head><title>Node.js</title></head><body><h1>Contact</h1><p>....</p></body></html>')
    // }

    if (req.url === "/api/students") {
        const students = [
            {
                id: 1,
                ime: "Janez",
                priimek: "Novak",
                smer: "ITK"
            },
            {
                id: 2,
                ime: "Mirko",
                priimek: "Krajnc",
                smer: "RIT"
            },
            {
                id: 3,
                ime: "Andrej",
                priimek: "Novak",
                smer: "ITP"
            }
        ];
        res.writeHead(200, { 'Content-type': 'application/json' });
        res.end(JSON.stringify(students));
    }

});

server.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`);
})